with import <nixpkgs> {}; {
  pyEnv = stdenv.mkDerivation {
    name = "chatterghost";
    buildInputs = [ stdenv python36Full python36Packages.virtualenv python36Packages.dbus-python ];
    LIBRARY_PATH="${libxml2}/lib:${python36Full}/lib";
    shellHook = ''
      unset http_proxy
      export GIT_SSL_CAINFO=/etc/ssl/certs/ca-bundle.crt
      export SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt
    '';
  };
}
