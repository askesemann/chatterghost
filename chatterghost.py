import notify2
import signal
from configparser import SafeConfigParser
from mattermostdriver import Driver
from time import sleep
import logging
import subprocess

log = logging.getLogger(__name__)

keep_running = None


def set_stop_flag(signum, frame):
    global keep_running
    keep_running = False


def get_driver_options():
    config = SafeConfigParser()
    config.read('chatterghost.cfg')
    driver_options = dict(
        url=config.get('mattermost', 'url'),
        login_id=config.get('mattermost', 'login_id'),
    )
    if config.has_option('mattermost', 'password'):
        driver_options['password'] = config.get('mattermost', 'password')
    if config.has_option('mattermost', 'password_command'):
        cmd = config.get('mattermost', 'password_command')
        password = subprocess.check_output(cmd.split(' ')).strip().decode("utf-8")
        driver_options['password'] = password
    if config.has_option('mattermost', 'scheme'):
        driver_options['scheme'] = config.get('mattermost', 'scheme')
    return driver_options


def show_notification(slc, pst):
    user = slc.users.get_user(pst['user_id'])
    n = notify2.Notification(
        "Incoming Message",
        "{0} wrote: {1}".format(
            user['username'], pst['message']),
        "notification-message-im"   # Icon name
    )
    n.show()
    log.debug("{0}: {1}".format(
              user['username'], pst['message']))


if __name__ == '__main__':
    exception = None
    slc = Driver(get_driver_options())
    slc.login()
    my_channels = slc.channels.get_channels_for_user(
        'me', 'wgii9zkod7nr8qjij99o98d18c')
    my_userid = slc.users.get_user('me')['id']
    my_username = slc.users.get_user(my_userid)['username']
    last_seen = {}
    keep_running = True
    notify2.init('chatterghost')
    signal.signal(signal.SIGTERM, set_stop_flag)

    while keep_running:
        try:
            for channel in my_channels:
                if not last_seen.get(channel['id']):
                    opts = dict(per_page=1)
                else:
                    opts = dict(after=last_seen.get(channel['id']))

                posts = slc.posts.get_posts_for_channel(channel['id'], opts)
                if posts['posts'].values() and last_seen.get(channel['id']):
                    for pst in posts['posts'].values():
                        if ((channel['type'] == 'D' or
                                my_username in pst['message'] or
                                '@channel' in pst['message'] or
                                '@all' in pst['message']) and
                                pst['user_id'] != my_userid):
                            show_notification(slc, pst)
                if posts['order']:
                    last_seen[channel['id']] = posts['order'][0]
            sleep(5)
        except KeyboardInterrupt:
            keep_running = False
